<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>PHP Arrays</title>
		<script type="text/javascript">
			var myArray= [];
			console.log(myArray);
			myArray[0]=1;
			myArray[1]=2;
			myArray[2]='hello';
			myArray[3]=3;
			myArray[4]=4;
			console.log(myArray);
			var vowelArray=['a','e','i','o','u'];
			console.log(vowelArray);
			console.log(vowelArray.length);
			vowelArray[20]='hi';
			console.log(vowelArray.length);
			console.log(vowelArray);
			console.log(vowelArray[16]);
		</script>
	</head>
	<body>
		<h1>JS Array length examples</h1>
		<h2>Look at the console</h2>
	</body>
</html>