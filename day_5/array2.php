<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>PHP Arrays</title>
		<script type="text/javascript">
			var nums=[10, 20, 30, 40, 50, 60];
			var fruits=['apple', 'orange', 'banana', 'apricot', 'mango', 'kiwi'];
			console.log(nums[3]);
			console.log(fruits[3]);
			var user= [];
			user['firstname']= 'Arshdeep';
			user['lastname']= 'Rangi';
			user['age']= '24';
			console.log("First name is " + user['firstname']);
			console.log("Last name is " + user['lastname']);
			console.log("Age is " + user['age']);
			console.log("\nnums array in reverse order :");
			for (var i = nums.length - 1; i >= 0; i--) {
				console.log(nums[i]);
			}
			console.log("\nnums array in stright order :");
			for (var i = 0; i <= nums.length - 1; i++) {
				console.log(nums[i]);
			}
			console.log("\nfruits array in reverse order :");
			for (var i = fruits.length - 1; i >= 0; i--) {
				console.log(fruits[i]);
			}
			console.log("\nfruits array in stright order :");
			for (var i = 0; i <= fruits.length - 1; i++) {
				console.log(fruits[i]);
			}
			console.log("\nfruits array in stright order using while loop :");
			var i=0;
			while(i<=fruits.length - 1){
				console.log(fruits[i]);
				i++;
			}
			console.log("\nfruits array in stright order using do-while loop :");
			var i=0;
			do{
				console.log(fruits[i]);
				i++;
			}while(i<=fruits.length - 1);
			console.log("\nfruits array using POP() Function :");
			var fruit;
			while(fruit=fruits.pop()){
				console.log(fruit);
			}

			var a='';
			for(var i=0; i<5; i++){
				a += "Ho ";
			}
			console.log(a);
			 
		</script>
	</head>
	<body>
		<h1>JS Array examples</h1>
		<h2>Look at the console</h2>
		<?php
			$fruits=['apple', 'orange', 'banana', 'apricot', 'mango', 'kiwi'];
			for ($i=0; $i < count(fruits) ; $i++) { 
				echo "\n";
				echo $fruits[i];
			}			
			$user = [];
			$user['firstname'] = 'Dave';
			$user['lastname'] = 'Jone';
			$user['age'] = 22;
			foreach($user as $key => $value) {
			echo "<p>" . $key . ': ' . $value . "</p>";
			}

			$fruits = ['apple', 'orange', 'banana', 'apricot', 'mango', 'kiwi'];
			foreach($fruits as $value) {
			echo "<br />" .$value ;
			}
		?>
	</body>
</html>