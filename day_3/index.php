<?php

$a=5;
$b=6;
$c=7;

echo "AND '&&' operator";
//FFF Should output false
if($a > $b && $b > $c){
	echo "<p>TRUE</p>";
}
else{
	echo "<p>False</p>";
}

//TTT should output true
if($a<$b && $b<$c){
	echo "<p>TRUE</p>";
}
else{
	echo "<p>False</p>";
}

echo "OR '||' operator";
//FFF Should output false
if($a > $b || $b > $c){
	echo "<p>TRUE</p>";
}
else{
	echo "<p>False</p>";
}

//TTT should output true
if($a<$b || $b<$c){
	echo "<p>TRUE</p>";
}
else{
	echo "<p>False</p>";
}

//TTFT should output true
if($a<$b || $b=$b || $b>$c){
	echo "<p>TRUE</p>";
}
else{
	echo "<p>False</p>";
}

//FFF should output true
if($a>$b || $b>$c){
	echo "<p>TRUE</p>";
}
else{
	echo "<p>False</p>";
}

if($a){
	echo "<p>TRUE</p>";
}
else{
	echo "<p>False</p>";
}

//XOR operator

if($a<$b | $b>$c){
	echo "<p>TRUE</p>";
}
else{
	echo "<p>False</p>";
}

if(FALSE xor TRUE){
	echo "<p>TRUE</p>";
}
else{
	echo "<p>False</p>";
}

if(TRUE | TRUE){
	echo "<p>TRUE</p>";
}
else{
	echo "<p>False</p>";
}