<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Truth</title>
	<script type="text/javascript">
		if('0'){
			console.log('String 0 is true');
		}
		else{
			console.log('String 0 is false');
		}

		if(null){
			console.log('NULL is true');
		}
		else{
			console.log('NULL is false');
		}

		if("null"){
			console.log('String NULL is true');
		}
		else{
			console.log('String NULL is false');
		}
	</script>
</head>
<body>
	<h1>Look at the console</h1>

	<?php
		if ('0') {
			echo('<p>String 0 is true</p>');
		}
		else{
			echo('<p>String 0 is false</p>');
		}
	?>
</body>
</html>